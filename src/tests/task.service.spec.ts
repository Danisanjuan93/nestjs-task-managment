import { NotFoundException } from '@nestjs/common';
import { MockFactory, Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Task } from '../tasks/entity/task.entity';
import { TaskStatus } from '../tasks/enum/task-status.enum';
import { TasksController } from '../tasks/tasks.controller';
import { TasksService } from '../tasks/tasks.service';

const mockUser = {
  username: 'Daniel',
  id: 'someId',
  password: 'somePassword',
  tasks: [],
};
type MockRepository<T = any> = Partial<Record<keyof Repository<T>, jest.Mock>>;

describe('TaskService', () => {
  let tasksService: TasksService;
  let tasksRepository: MockRepository<Repository<Task>>;

  const repositoryMockFactory: () => any = jest.fn(() => ({
    findOne: jest.fn((entity) => entity),
    findAndCount: jest.fn((entity) => entity),
    create: jest.fn((entity) => entity),
    save: jest.fn((entity) => entity),
    update: jest.fn((entity) => entity),
    delete: jest.fn((entity) => entity),
    createQueryBuilder: jest.fn(() => ({
      delete: jest.fn().mockReturnThis(),
      innerJoinAndSelect: jest.fn().mockReturnThis(),
      innerJoin: jest.fn().mockReturnThis(),
      from: jest.fn().mockReturnThis(),
      where: jest.fn().mockReturnThis(),
      andWhere: jest.fn().mockReturnThis(),
      execute: jest.fn().mockReturnThis(),
      getOne: jest.fn(),
      getMany: jest.fn(() => []),
    })),
  }));

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      controllers: [TasksController],
      providers: [
        TasksService,
        {
          provide: getRepositoryToken(Task),
          useFactory: repositoryMockFactory,
        },
      ],
    }).compile();

    tasksService = module.get<TasksService>(TasksService);
    tasksRepository = module.get(getRepositoryToken(Task));
  });

  describe('getTasks', () => {
    it('calls getTasks', async () => {
      const tasks2 = await tasksService.getTasks(null, mockUser);
      expect(tasks2).not.toBeUndefined();
      expect(tasks2.length).toEqual(0);

      const queryBuilder: any = jest.fn(() => ({
        where: jest.fn().mockReturnThis(),
        andWhere: jest.fn().mockReturnThis(),
        getMany: jest.fn().mockResolvedValueOnce([
          {
            id: '1',
            title: '1',
            description: '1',
            status: TaskStatus.DONE,
            user: mockUser,
          },
        ]),
      }));
      tasksRepository.createQueryBuilder.mockImplementation(queryBuilder);
      const tasks1 = await tasksService.getTasks(null, mockUser);

      expect(tasks1).not.toBeUndefined();
      expect(tasks1.length).toEqual(1);
    });

    it('calls getTaskById', async () => {
      tasksRepository.findOne.mockResolvedValueOnce(null);
      try {
        await tasksService.getTaskById('1', mockUser);
      } catch (error) {
        expect(error).toEqual(
          new NotFoundException('Task with id: 1 was not found'),
        );
      }

      tasksRepository.findOne.mockResolvedValueOnce({
        id: '1',
        title: '1',
        description: '1',
        status: TaskStatus.DONE,
        user: mockUser,
      });
      const task1 = await tasksService.getTaskById('1', mockUser);
      expect(task1).not.toBeUndefined();
      expect(task1.id).toEqual('1');
    });
  });
});
