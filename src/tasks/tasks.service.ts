import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../auth/entity/user.entity';
import { Repository } from 'typeorm';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { Task } from './entity/task.entity';
import { TaskStatus } from './enum/task-status.enum';

@Injectable()
export class TasksService {
  constructor(
    @InjectRepository(Task)
    private tasksRepository: Repository<Task>,
  ) {}

  async getTasks(filters: GetTasksFilterDto, user: User): Promise<Task[]> {
    const { status, search } = filters || {};
    const query = this.tasksRepository.createQueryBuilder('task');

    query.where({ user });

    if (status) {
      query.andWhere('task.status = :status', { status });
    }
    if (search) {
      query.andWhere(
        '(task.title LIKE LOWER(:search) OR task.description LIKE LOWER(:search))',
        { search: `%${search}%` },
      );
    }

    return query.getMany();
  }

  async getTaskById(id: string, user: User): Promise<Task> {
    const task: Task = await this.tasksRepository.findOne({
      where: { id, user },
    });

    if (!task) throw new NotFoundException(`Task with id: ${id} was not found`);
    return task;
  }

  async createTask(createTaskDto: CreateTaskDto, user: User): Promise<Task> {
    const newData: Partial<Task> = {
      ...createTaskDto,
      status: TaskStatus.OPEN,
      user,
    };
    const task: Task = this.tasksRepository.create(newData);

    await this.tasksRepository.save(task);
    return task;
  }

  async updateTask(
    id: string,
    status: TaskStatus,
    user: User,
  ): Promise<boolean> {
    const taskToUpdate = await this.getTaskById(id, user);
    taskToUpdate.status = status;
    const updateResult = await this.tasksRepository.update(id, taskToUpdate);
    return updateResult.affected > 0;
  }

  async deleteTask(id: string, user: User): Promise<boolean> {
    const deleted = await this.tasksRepository.delete({ id, user });

    if (deleted.affected === 0)
      throw new NotFoundException(`Task with id: ${id} was not found`);

    return true;
  }
}
