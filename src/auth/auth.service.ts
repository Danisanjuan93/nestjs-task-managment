import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { Repository } from 'typeorm';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { User } from './entity/user.entity';
import { AuthLoginResponse } from './interface/auth-responses.interface';
import { JwtPayload } from './interface/jwt.interface';
import { PsqlCodeErrors } from './utils/psql-code-errors.utils';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    private jwtService: JwtService,
  ) {}

  async getUserByUsername(username: string): Promise<User> {
    const user = await this.usersRepository.findOneBy({ username });

    return user;
  }

  async login(credentials: AuthCredentialsDto): Promise<AuthLoginResponse> {
    const { username, password } = credentials;
    const user = await this.getUserByUsername(username);
    if (user && (await bcrypt.compare(password, user.password))) {
      const payload: JwtPayload = { username };
      const accessToken = this.jwtService.sign(payload);
      return { accessToken };
    } else {
      throw new UnauthorizedException('Invallid login credentials');
    }
  }

  async createUser(credentials: AuthCredentialsDto): Promise<void> {
    const { username, password } = credentials;

    const salt = await bcrypt.genSalt();
    const hashedPassword = await bcrypt.hash(password, salt);

    const user: User = this.usersRepository.create({
      username,
      password: hashedPassword,
    });
    try {
      await this.usersRepository.save(user);
    } catch (error) {
      const exceptionThrown = PsqlCodeErrors[error.code];
      throw exceptionThrown ? exceptionThrown : PsqlCodeErrors[99999];
    }
  }
}
