import {
  ConflictException,
  InternalServerErrorException,
} from '@nestjs/common';

export const PsqlCodeErrors = {
  99999: new InternalServerErrorException('Error saving data into database'),
  23505: new ConflictException('Username already exists'),
};
