import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthCredentialsDto } from './dto/auth-credentials.dto';
import { AuthLoginResponse } from './interface/auth-responses.interface';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('/register')
  register(@Body() credentialsDto: AuthCredentialsDto): Promise<void> {
    return this.authService.createUser(credentialsDto);
  }

  @Post('/login')
  login(
    @Body() credentialsDto: AuthCredentialsDto,
  ): Promise<AuthLoginResponse> {
    return this.authService.login(credentialsDto);
  }
}
