import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export const DataBaseConfig: TypeOrmModuleOptions = {
  type: 'postgres',
  host: 'host.docker.internal',
  port: 54320,
  username: 'admin',
  password: 'admin',
  database: 'task-management',
  autoLoadEntities: true,
  synchronize: true,
  entities: [__dirname + 'entity/*.entity.ts'],
};
